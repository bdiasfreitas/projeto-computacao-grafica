# Unlearn
It's a 2.5D plataform game where the main character is trapped in his dreams.  
The player needs to get out before the light is over.
This project was created for my Computer Graphic Project class taken at Univerisdade Federal do Rio Grande do Sul (UFRGS).

# Current Progress
[Demo Video](https://youtu.be/PxyLTyCM2IQ)


# Technologies
* Unity Engine (version 2017.2.0f3)
* C# scripts
* Visual Studio 2017

# Assets
* [Yughues Free Pavement Materials](https://assetstore.unity.com/packages/2d/textures-materials/roads/yughues-free-pavement-materials-12952)
* [Deucalion's Creatures](https://assetstore.unity.com/packages/3d/characters/creatures/deucalion-s-creatures-95771)
* [Modular Medieval Lanterns](https://assetstore.unity.com/packages/3d/environments/historic/modular-medieval-lanterns-85527)
* [Free Sound FX](https://assetstore.unity.com/packages/audio/sound-fx/free-sound-fx-31837)
* [Warped Fantasy Music Pack](https://assetstore.unity.com/packages/audio/ambient/fantasy/warped-fantasy-music-pack-49914)

# Contact
**Email:** bdiasfreitas@gmail.com