﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeDistortion : MonoBehaviour {

    public Vector3 distortionSpeed;
    public GameObject cubeObject;

    private bool startDistortion = false;
    private ReCalcCubeTexture reCalcScript;

	// Use this for initialization
	void Start ()
    {
        reCalcScript = cubeObject.GetComponent<ReCalcCubeTexture>();
    }
	
	// Update is called once per frame
	void Update () {
		if(startDistortion)
            cubeObject.transform.localScale += distortionSpeed;
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            startDistortion = true;
            reCalcScript.enabled = false;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            startDistortion = false;
            reCalcScript.enabled = true;
        }
    }
}
