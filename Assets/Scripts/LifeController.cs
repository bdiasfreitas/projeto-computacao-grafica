﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeController : MonoBehaviour {

    private int childrenTotal;
    private int childrenEnabled;
    private GameController gameController;

    private void Start()
    {
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        childrenTotal = transform.childCount;
        childrenEnabled = childrenTotal;
    }

    public void RemoveLife()
    {
        if(childrenEnabled > 0)
        {
            childrenEnabled--;

            transform.GetChild(childrenEnabled).gameObject.SetActive(false);

            if (childrenEnabled == 0)
            {
                gameController.GameOver();
            }
        }
    }
}
