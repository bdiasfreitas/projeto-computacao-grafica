﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour {
    public float lightBonus = 0.5f;

    private GameObject lampObject;
    private Light mainLight;
    private AudioSource audioClip;    

    void Start()
    {

        lampObject = GetComponent<GameObject>();
        mainLight = GameObject.FindGameObjectWithTag("Main Light").GetComponent<Light>();
        audioClip = GameObject.FindGameObjectWithTag("Lamp Audio").GetComponent<AudioSource>();
        if (mainLight == null)
            Debug.Log("Não foi encontrado a luz do personagem!");
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            audioClip.Play();
            mainLight.range = mainLight.range + lightBonus;
            DestroyObject(transform.gameObject);
        }
    }
}
