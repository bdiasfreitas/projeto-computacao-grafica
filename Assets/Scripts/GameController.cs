﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GameController : MonoBehaviour {

    public float resurrectionTime = 2;

    private GameObject player;
    private CameraFollow mainCameraScript; 
    private Vector3 checkPoint;

    private LifeController lifeController;
    private UnityEngine.UI.Text informationText;

    private bool playerDied = false;
    private float diedTime = 0;
    private AudioSource playerScream;
    private Animator playerAnimator;

    private bool gameOver = false;

    private List<EnemyAttack> enemyScripts;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerAnimator = player.GetComponent<Animator>();
        mainCameraScript = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraFollow>();
        lifeController = GameObject.FindGameObjectWithTag("LifeController").GetComponent<LifeController>();
        informationText = GameObject.FindGameObjectWithTag("InformationText").GetComponent<UnityEngine.UI.Text>();
        playerScream = GameObject.FindGameObjectWithTag("PlayerScream").GetComponent<AudioSource>();

        enemyScripts = GameObject.FindGameObjectsWithTag("Enemy").Select(s => s.GetComponent<EnemyAttack>()).ToList();
        
        checkPoint = new Vector3(0, 0, 0);
    }

    private void Update()
    {
        if(playerDied && !gameOver)
        {
            if(Time.realtimeSinceStartup > diedTime + resurrectionTime)
            {
                playerDied = false;
                player.transform.position = checkPoint;
                mainCameraScript.StartFollow();
                lifeController.RemoveLife();
                playerAnimator.SetBool("IsDead", false);
            }
        }
        else if(gameOver)
        {
            player.SetActive(false);
        }
    }

    public void PlayerDied()
    {
        playerDied = true;
        diedTime = Time.realtimeSinceStartup;
        playerScream.Play();
        playerAnimator.SetBool("IsDead", true);
    }

    public void PlayerDamaged()
    {
        //player.transform.position = checkPoint;
        enemyScripts.ForEach(s => s.StopAttack());
        PlayerDied();
    }


    public void SetCheckpoint(Vector3 checkpointPosition)
    {
        checkPoint = checkpointPosition;
    }

    public void GameOver()
    {
        gameOver = true;
        informationText.text = "Game Over";
        player.SetActive(false);
    }

    public bool IsPlayerDied()
    {
        return playerDied;
    }
}
