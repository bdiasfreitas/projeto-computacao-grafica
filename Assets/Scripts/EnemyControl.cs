﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControl : MonoBehaviour {

    public bool walk = false;
    public int stoppedAction = 5;
    public int speed = 1;

    private Animator myAnimator;
    private bool crouched;
    private bool attack = false;
    private bool attacking = false;

    private AudioSource audioSource;

    private GameController gameController;

	void Start () {
        myAnimator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();

        crouched = !walk;
        attack = false;
	}
	
	// Update is called once per frame
	void Update () {
        //if (!gameController.IsPlayerDied())
        //{
            if (attack)
            {
                if (crouched)
                    myAnimator.SetInteger("CurrentAction", 0);

                myAnimator.SetFloat("VSpeed", 0);
                attacking = true;
                crouched = false;
                myAnimator.SetLayerWeight(1, 1f);
                myAnimator.SetInteger("CurrentAction", 4);
            }
            else if (walk)
            {
                if (crouched || attacking)
                {
                    myAnimator.SetInteger("CurrentAction", 0);
                    audioSource.Play();
                }

                crouched = false;
                myAnimator.SetFloat("VSpeed", speed);
            }
            else
            {
                if (attacking)
                    myAnimator.SetInteger("CurrentAction", 0);

                attacking = false;
                crouched = true;
                myAnimator.SetLayerWeight(1, 1f);
                myAnimator.SetInteger("CurrentAction", stoppedAction);
            }
        //}
        //else
        //{
        //    if (attacking)
        //        myAnimator.SetInteger("CurrentAction", 0);

        //    attacking = false;
        //    crouched = true;
        //    myAnimator.SetLayerWeight(1, 1f);
        //    myAnimator.SetInteger("CurrentAction", stoppedAction);
        //}
	}

    public void SetWalk(bool walk)
    {
        this.walk = walk;
    }

    public void Destroy()
    {
        Destroy(this.gameObject);
    }

    internal void SetAttack(bool attack)
    {
        this.attack = attack;
    }
}
