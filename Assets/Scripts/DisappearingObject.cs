﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisappearingObject : MonoBehaviour {

    public float visibleTime;
    public float invisibleTime;

    private bool visible;    
    private float lastChange;

    private BoxCollider objectCollider;
    private MeshRenderer objectRenderer;

	void Start () {
        objectCollider = GetComponent<BoxCollider>();
        objectRenderer = GetComponent<MeshRenderer>();
	}
	
	void Update () {
        float timeDiff = Time.realtimeSinceStartup - (lastChange + (visible ? visibleTime : invisibleTime));

        if (timeDiff >= 0)
            ChangeStatus();
	}

    void ChangeStatus()
    {
        visible = !visible;
        objectCollider.enabled = visible;
        objectRenderer.enabled = visible;
        lastChange = Time.realtimeSinceStartup;
    }
}
