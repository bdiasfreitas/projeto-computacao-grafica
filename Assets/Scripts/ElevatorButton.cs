﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorButton : MonoBehaviour {

    public Vector3 finalPosition;
    public float speed;

    private float originalSpped;

    private GameObject player;
    private bool moving = false;
    private bool startedOnInitialPosition = true;
    private Vector3 initialPosition;

    private BoxCollider boxCollider;

    private GameController gameController;

    void Start()
    {
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        originalSpped = speed;

        boxCollider = GetComponent<BoxCollider>();
        initialPosition = transform.parent.position;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            player = other.gameObject;
            player.GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl>().Disable();
            moving = true;
            transform.position += new Vector3(0, -0.07f, 0);
            boxCollider.size += new Vector3(1, 1, 1);
        }
    }

    void Update()
    {
        if(gameController.IsPlayerDied())
        {
            startedOnInitialPosition = true;
            moving = false;
            transform.parent.position = initialPosition;
            speed = originalSpped;
        }
        else if(moving)
        {
            var vectorSpeed = (finalPosition - initialPosition) * speed; 
            
            transform.parent.position += vectorSpeed;
            if(player != null)
                player.transform.position += vectorSpeed;

            if(VerifyLimit(transform.parent.position, vectorSpeed, startedOnInitialPosition ? finalPosition : initialPosition))
            {
                startedOnInitialPosition = !startedOnInitialPosition;
                moving = !moving;
                speed *= -1;
                if (player != null)
                    player.GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl>().Enable();
            }
        }
    }

    private bool VerifyLimit(Vector3 currentPos, Vector3 speed, Vector3 finalPos)
    {
        return VerifyFloatLimit(currentPos.x, speed.x, finalPos.x)
            || VerifyFloatLimit(currentPos.y, speed.y, finalPos.y)
            || VerifyFloatLimit(currentPos.z, speed.z, finalPos.z);
    }

    private bool VerifyFloatLimit(float currentPos, float speed, float finalPos)
    {
        if (speed > 0)
            return currentPos + speed > finalPos;
        else
            return currentPos + speed < finalPos;
    }
    
    void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            player.GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl>().Enable();
            player = null;
            transform.position += new Vector3(0, +0.07f, 0);
            boxCollider.size += new Vector3(-1, -1, -1);
        }
    }
}
