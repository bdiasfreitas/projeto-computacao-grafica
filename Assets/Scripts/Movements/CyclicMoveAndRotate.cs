﻿using System;
using UnityEngine;

namespace UnityStandardAssets.Utility
{
    public class CyclicMoveAndRotate : MonoBehaviour
    {
        public Vector3andSpace moveUnitsPerSecond;        
        public Vector3andSpace rotateDegreesPerSecond;

        public bool ignoreTimescale;
        private float m_LastRealTime;

        private Vector3 startedPosition;
        private Collider player = null;

        private void Start()
        {
            m_LastRealTime = Time.realtimeSinceStartup;
            startedPosition = transform.position;
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.tag.Equals("Player"))
            {
                player = other;
            }
        }

        void OnTriggerExit(Collider other)
        {
            if (other.tag.Equals("Player"))
            {
                player = null;
            }
        }


        // Update is called once per frame
        private void Update()
        {
            float deltaTime = Time.deltaTime;
            if (ignoreTimescale)
            {
                deltaTime = (Time.realtimeSinceStartup - m_LastRealTime);
                m_LastRealTime = Time.realtimeSinceStartup;
            }
            VerifyDirections();
            transform.Translate(moveUnitsPerSecond.value * deltaTime, moveUnitsPerSecond.space);
            if (player != null)
            {
                //var rotation = player.transform.rotation;
                //player.transform.Translate(new Vector3(0, moveUnitsPerSecond.value.z, moveUnitsPerSecond.value.x) * deltaTime, player.transform);
                player.transform.position = player.transform.position + (new Vector3(moveUnitsPerSecond.value.x, moveUnitsPerSecond.value.y, 0) * deltaTime);
            }
            transform.Rotate(rotateDegreesPerSecond.value * deltaTime, moveUnitsPerSecond.space);
        }

        private void VerifyDirections()
        {
            VerifyLimit(startedPosition.x, transform.position.x, moveUnitsPerSecond.max.x, ref moveUnitsPerSecond.value.x);
            VerifyLimit(startedPosition.y, transform.position.y, moveUnitsPerSecond.max.y, ref moveUnitsPerSecond.value.y);
            VerifyLimit(startedPosition.z, transform.position.z, moveUnitsPerSecond.max.z, ref moveUnitsPerSecond.value.z);
        }

        private void VerifyLimit(float initialPos, float currentPos, float max, ref float movement)
        {
            if ((Math.Abs(initialPos) + Math.Abs(max) < Math.Abs(currentPos) && movement > 0)
                || (Math.Abs(initialPos) - Math.Abs(max) > Math.Abs(currentPos) && movement < 0))
                movement *= -1;
        }


        [Serializable]
        public class Vector3andSpace
        {
            public Vector3 value;
            public Vector3 max;
            public Space space = Space.Self;
        }
    }
}
