﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTrigger : MonoBehaviour {

    EnemyControl enemyScript;
    public GameObject enemy;

    void Start()
    {
        //GameObject enemy = GameObject.FindGameObjectWithTag("Enemy");
        if (enemy != null)
            enemyScript = (EnemyControl)enemy.GetComponent(typeof(EnemyControl));
    }

	void OnTriggerEnter(Collider other)
    {
        
        if(other.tag.Equals("Player"))
        {
            if (enemyScript != null)
                enemyScript.SetWalk(true);
        }
    }

    void OnTriggerExit(Collider other)
    {

        if (other.tag.Equals("Player"))
        {
            if (enemyScript != null)
                enemyScript.SetWalk(false);
        }
        else if(other.tag.Equals("Enemy"))
        {
            if (enemyScript != null)
                enemyScript.Destroy();
        }

    }
}
