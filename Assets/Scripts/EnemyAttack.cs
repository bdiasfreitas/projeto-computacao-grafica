﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour {

    EnemyControl enemyScript;
    public GameObject enemy;

    private SphereCollider sphereCollider;

    private GameController gameController;

    private float originalZ;

    void Start () 
    {
        originalZ = transform.position.z;
        //GameObject enemy = GameObject.FindGameObjectWithTag("Enemy");
        if (enemy != null)
            enemyScript = (EnemyControl)enemy.GetComponent(typeof(EnemyControl));
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();

        sphereCollider = GetComponent<SphereCollider>();

    }
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(transform.position.x, transform.position.y,  originalZ);
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag.Equals("Player"))
        {
            if (!gameController.IsPlayerDied())
            {
                if (enemyScript != null)
                    enemyScript.SetAttack(true);

                sphereCollider.radius += 0.2f;
            }
            else
                enemyScript.SetAttack(false);
        }
    }    

    void OnTriggerExit(Collider other)
    {
        if(other.tag.Equals("Player"))
        {
            if (enemyScript != null)
                enemyScript.SetAttack(false);

            sphereCollider.radius -= 0.2f;
        }
    }


    public void StopAttack()
    {
        enemyScript.SetAttack(false);

        //sphereCollider.radius -= 0.2f;
    }

}
