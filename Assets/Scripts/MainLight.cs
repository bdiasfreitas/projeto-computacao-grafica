﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainLight : MonoBehaviour {

    public float changeSpeed;

    private Light lightComp;
    private UnityEngine.UI.Slider lightMeter;
    
	// Use this for initialization
	void Start () {
        lightComp = GetComponent<Light>();
        if (lightComp == null)
            Debug.Log("Não foi encontrado o componente da luz!");

        lightMeter = GameObject.FindGameObjectWithTag("Light Meter").GetComponent<UnityEngine.UI.Slider>();
	}
	
	void Update () {
        if (this.tag.Equals("Main Light"))
        {
            float range = lightComp.range + changeSpeed * Time.deltaTime;
            lightComp.range = range;

            float slideValue = (range) / 10f * 100f;
            lightMeter.value = slideValue > 100f ? 100f : slideValue;

            if (slideValue <= 0)
                GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().GameOver();
        }
    }
}
