﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DieByBoundary : MonoBehaviour {

    private GameObject player;
    private CameraFollow mainCameraScript;
    private CameraFollow mainLightScript;
    private GameController gameController;

    private void Start()
    {
        mainCameraScript = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraFollow>();
        mainLightScript = GameObject.FindGameObjectWithTag("Main Light").GetComponent<CameraFollow>();
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag.Equals("Player"))
        {
            player = other.gameObject;
            mainCameraScript.StopFollow();
            gameController.PlayerDied();
        }
    }
}
